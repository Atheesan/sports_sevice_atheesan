-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2013 at 09:16 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fourm`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE IF NOT EXISTS `applications` (
  `userid` int(11) NOT NULL,
  `tournamentid` int(11) NOT NULL,
  `applicationpath` varchar(255) NOT NULL,
  PRIMARY KEY (`userid`,`tournamentid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`userid`, `tournamentid`, `applicationpath`) VALUES
(1, 3, 'applications/1_assignment1.docx'),
(1, 6, 'applications/1_Bieber-Justin.jpg'),
(2, 1, 'applications/2_adminarea.php'),
(2, 2, 'applications/2_Document.java'),
(3, 2, 'applications/3_Designing basic model and E.doc'),
(3, 4, 'applications/3_Functional requirements.docx');

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE IF NOT EXISTS `notices` (
  `userid` int(11) NOT NULL,
  `tournamentid` int(11) NOT NULL,
  `readnotice` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`,`tournamentid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`userid`, `tournamentid`, `readnotice`) VALUES
(1, 1, 1),
(1, 3, 0),
(1, 4, 0),
(1, 5, 0),
(1, 6, 0),
(1, 7, 0),
(1, 8, 0),
(2, 1, 1),
(2, 2, 1),
(3, 2, 1),
(3, 3, 1),
(3, 4, 1),
(3, 5, 1),
(3, 6, 0),
(3, 7, 0),
(3, 8, 0),
(4, 1, 1),
(4, 2, 0),
(4, 3, 0),
(4, 4, 0),
(4, 5, 0),
(4, 6, 0),
(4, 7, 0),
(4, 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tournaments`
--

CREATE TABLE IF NOT EXISTS `tournaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT 'tournament organizer',
  `applicationform_path` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `datetime` datetime NOT NULL,
  `venue` varchar(50) NOT NULL,
  `no_of_participants` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tournaments`
--

INSERT INTO `tournaments` (`id`, `userid`, `applicationform_path`, `name`, `datetime`, `venue`, `no_of_participants`, `description`) VALUES
(1, 3, 'uploads/fx100AUpdf.pdf', 'final', '2013-01-01 01:01:00', 'University of Moratuwa', 5, 'apply for this'),
(2, 1, 'uploads/10032.jpg', 'Morning test', '2013-01-01 01:01:00', 'University of Moratuwa', 5, 'test'),
(3, 2, 'uploads/cars_2 (1).doc', 'Morning test 2', '2013-01-01 01:01:00', 'University of Moratuwa', 10, 'this is a test mail'),
(4, 2, 'uploads/temp.txt', 'final morning test', '2013-01-01 01:01:00', 'University of Moratuwa', 12, 'test'),
(5, 2, 'uploads/temp (3).txt', 'final final test', '2013-01-01 01:01:00', 'University of Moratuwa', 15, 'final test'),
(6, 2, 'uploads/Analysis.docx', '', '2013-11-02 08:30:00', 'UOM', 10, 'hdggshk'),
(7, 2, 'uploads/10032.jpg', 'yyty', '2013-01-01 01:01:00', 'ghgh', 0, 'ghghg'),
(8, 2, 'uploads/10032.jpg', 'yyty', '2013-01-01 01:01:00', 'ghgh', 0, 'ghghg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(8) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_date` datetime NOT NULL,
  `user_level` int(8) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name_unique` (`user_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_pass`, `user_email`, `user_date`, `user_level`) VALUES
(1, 'sandun', '8e2bd7517bf60076855b43680e0a8ee79487d358', 'sandun@gmail.com', '2013-10-23 11:15:58', 0),
(2, 'ruwanka', '087c46d3c9f77605b68127da779551685caae22e', 'ruwankamadhushan@gmail.com', '2013-10-23 14:20:22', 0),
(3, 'ama', 'd8e6e1405e607479c1ce78791f76a05cb6dc01fa', 'i.yomee90@gmail.com', '2013-10-23 14:45:08', 0),
(4, 'chirantha', 'e60c6efb46d351ca49310b995a878e0b7a6f6583', 'visionnemo@gmail.com', '2013-10-23 15:00:58', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
