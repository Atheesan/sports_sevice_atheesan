<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Sports Service</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/func.js"></script>
<script type="text/javascript" src="js/slidesw.js"></script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <div class="img"><img src="images/head.png" width="420" height="100" alt="" class="fl" /></div>
      </div>
      
      <div class="reg">
        <ul>
         
        </ul>
      </div>
   <div class="clr"></div>

      <div class="menu_nav">
        <ul>
       <li><a href="/CAll/Club/index.php"><span>Home</span></a></li>
		<li><a href="/CAll/Club/application.php"><span>Apply</span></a></li>
		</ul>
      </div>
      <div class="clr"></div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article1">
          <h2><b><span>Organize Tournament</span> </b>         </h2>
          <div class="clr"></div>
          
            <p>Fill the form and invite all members to the tournament.</br>

<?php
include 'connect.php';
include('header.php');
include ('head.php');



if(isset($_GET['err']) && $_GET['err'] == 0 && (isset($_GET['sentmails']) && isset($_GET['failedmails']))){
	echo '<h4>Tournament was successfuly added. '.$_GET['sentmails'].' Emails were sent to other users.</h4>';
//	echo '<h5>Sending emails failed to '.$_GET['failedmails'].' users</h5>';
	}else if(isset($_GET['err']) && ($_GET['err'] < 6 && $_GET['err'] > 0)){
		echo '<h4>Something went wrong! please try again.</h4>';
		}
?>
<div class="club">

<table>
<form action="tourprocess.php" enctype="multipart/form-data" method="post">

<tr><td><label for="regform">Upload Registration form</label></td>
<td><input type="file" name="regForm" /></td></tr>

<tr><td><label for="tournamentname">Tournament Name</label></td>
<td><input type="text" name="tournamentname" /></td></tr>

<tr><td><label for="date">Date</label></td>
<td>
<?php 
echo '<select name="year">';
for($i = 2013; $i < 2051; $i++){
	echo '<option>'.$i.'</option>';
	}
echo '</select>';
echo '<select name="month">';
for($i = 1; $i < 13; $i++){
	if($i < 10){
		echo '<option>0'.$i.'</option>';
		}else{
		echo '<option>'.$i.'</option>';
		}
	}
echo '</select>';
echo '<select name="date">';
for($i = 1; $i < 32; $i++){
	if($i < 10){
		echo '<option>0'.$i.'</option>';
		}else{
		echo '<option>'.$i.'</option>';
		}
	}
echo '</select>';
?>
</td></tr>

<tr><td><label for="time">Time</label></td>
<td>
<?php
echo '<select name="hour">';
for($i = 1; $i < 25; $i++){
	if($i < 10){
		echo '<option>0'.$i.'</option>';
		}else{
		echo '<option>'.$i.'</option>';
		}
	}
echo '</select>';
echo '<select name="minute">';
for($i = 1; $i < 61; $i++){
	if($i < 10){
		echo '<option>0'.$i.'</option>';
		}else{
		echo '<option>'.$i.'</option>';
		}
	}
echo '</select>';
?>
</td></tr>

<tr><td><label for="venue">Venue</label></td>
<td><input type="text" name="venue" /></td></tr>

<tr><td><label for="noofparticipants">No Of Participants Per Team</label></td>
<td><input type="text" name="noofparticipants" /></td></tr>

<tr><td><label for="desc">Tournament Description</label></td>
<td><textarea name="desc"></textarea></td></tr>

<tr><td><input type="submit" value="submit" /></td></tr>
</form>
</table>

</div>
<div class="box">
<h2>Received Applications</h2>
<?php
$sql = "SELECT * FROM tournaments, applications, friendship_system_users_table WHERE tournaments.userid = '.$user_id.' AND applications.tournamentid = tournaments.id AND applications.userid = friendship_system_users_table.id";
$result = mysql_query($sql, $connection);
if($result && mysql_num_rows($result) > 0){
	while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
		//var_dump($row);
		echo '<p><strong>Application for tournament:</strong> '.$row['name'].' <strong>Applier name:</strong> '.$row['username'].' <a href="'.$row['applicationpath'].'">Download application</a></p>';
		}
	}else{
		echo 'no applications for previous tournaments'.mysql_error();
		}
		
?>
</div>
            
         
          <div class="clr"></div>
        </div>
      </div>
      <div class="sidebar">
        <div class="searchform"></div>
 		
        <div class="img"><img src="images/logo.jpg" width="261" height="86" alt="" class="fl" /></div>
        
        <div class="clr"></div>
       <div class="gadget">
          <h2 class="star"><span>Club</span></h2>
          <div class="clr"></div>
          <ul class="sb_menu">
             <li><a href="/CAll/Club/index.php"><span>Send Application</span></a></li>
		<li><a href="/CAll/Club/application.php"><span>Apply</span></a></li>
          </ul>
        </div>
        <div class="gadget">
          <h2 class="star"><span>Users</span></h2>
          <div class="clr"></div>
          <ul class="ex_menu">
            <li><a href="/CAll/Coach/index.php">Coach</a></li>
            <li><a href="/CAll/Student/index.php">Student</a></li>
            <li><a href="/CAll/Sportsman/index.php">Sportsman</a></li>
            <li><a href="/CAll/Club/index.php">Club</a></li>
          </ul>
           <div class="clr"></div>
        <div class="img"><img src="images/img1.jpg" width="220" height="215" alt="" class="fl" /></div>
        </div>
       
      </div>
      <div class="clr"></div>
    </div>
  </div>
 <div class="fbg">
    <div class="fbg_resize">
      <div class="col c1">
        <h2><span>Image</span> Gallery</h2>
        <a href="#"><img src="images/gal1.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal2.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal3.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal4.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal5.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal6.jpg" width="75" height="75" alt="" class="gal" /></a> </div>
      <div class="col c2">
         <h2><span>Our Services</h2>
        <p>We provide our best servicers to you.</p>
        <ul class="fbg_ul">
          <li><a href="#">Invite friends to play</a></li>
          <li><a href="#">Arrange tournaments</a></li>
          <li><a href="#">Find your coach</a></li>
        </ul>
      </div>
      <div class="col c3">
            <h2><span>Contact</span> Us</h2>
        <p>We are here to provide our best service to you.</p>
        <p class="contact_info">
           <span>Name : </span>Ridgecrest Asia (Pvt) Ltd.<br /> 
          <span>Address:</span>113, 5th Lane, Colombo 03.<br />
          <span>Telephone:</span>0094 11 2370876 <br />
          <span>FAX:</span>0094 11 2370878<br />
          <span>E-mail:</span> <a href="#">info@ridgecrest.lk</a> </p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="footer">
    <div class="footer_resize">
      <p class="lf">&copy; Copyright © 2013<a href="http://www.sportslk.com"> Ridgecrest, Inc. </a>All rights reserved.</p>
      <p class="rf">Design by Ridgecrest</p>
      <div style="clear:both;"></div>
    </div>
  </div>
</div>
<div class="end">
<div class="end_text">© 2013<a href="http://www.sportslk.com"> Ridgecrest, Inc. </a>All rights reserved.</div>
</body>
</html>
