  <div id="menuBar">
    <div id="shortcutWrapper">
      <div id="navigationbar">
        <div id='cssmenu'>
          <ul>
            <li class='has-sub'><a href='#File'><span>File</span></a>
              <ul>
                <li><a href='#New'><span><img class="menuimg" src="images/newFile.png" style="width: 15px ; height: 16px"/>&nbsp;&nbsp; New File</span></a></li>
                <li><a href='#OpenFile'><span><img class="menuimg" src="images/openfile.png" style="width:15px ; height:16px"/>&nbsp;&nbsp; Open File</span></a></li>
                <li onclick= "msgboxORF()"><a href='script.js' ><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Open Recent File</span></a></li>
                <li><a href='#SaveAs'><span><img class="menuimg" src="images/saveas.png" style="width:15px ; height: 17px"/>&nbsp;&nbsp; Save As</span></a></li>
                <li><a href='#Save'><span><img class="menuimg" src="images/save.png" style="width: 15px ; height: 17px"/>&nbsp;&nbsp; Save</span></a></li>
                <li class='last'><a href='#Exit'><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Exit</span></a></li>
              </ul>
            </li>
            <li class='has-sub'><a href='#Edit'><span>Edit</span></a>
              <ul>
                <li><a href='#Undo'><span>Undo</span></a></li>
                <li class='last'><a href='#Redo'><span>Redo</span></a></li>
              </ul>
            </li>
            <li class='has-sub'><a href='#tool'><span>Tool</span></a>
              <ul>
                <li><a href='#Undo'><span></span></a></li>
                <li class='last'><a href='#Redo'><span>Redo</span></a></li>
              </ul>
            </li>
            <li class='has-sub'><a href='#help'><span>Help</span></a>
              <ul>
                <li><a href='#Undo'><span>Undo</span></a></li>
                <li class='last'><a href='#Redo'><span>Redo</span></a></li>
              </ul>
            </li>
            <li class='has-sub'><a href='#'><span>Item</span></a>
              <ul>
                <li><a href='#Undo'><span>Undo</span></a></li>
                <li class='last'><a href='#Redo'><span>Redo</span></a></li>
              </ul>
            </li>
            <li class='has-sub'><a href='#'><span>Item</span></a>
              <ul>
                <li><a href='#Undo'><span>Undo</span></a></li>
                <li class='last'><a href='#Redo'><span>Redo</span></a></li>
              </ul>
            </li>
            <li class='has-sub'><a href='#'><span>Item</span></a></li>
            <li class='has-sub'><a href='#'><span>Item</span></a></li>
          </ul>
        </div>
		<div id="logodiv">
		<h4>DMS FIT</h4>
		<h6>log out</h6>
		</div>
      </div>
      <div id="toolBar">
        <input type="button" class="classname" id="btnupload" value="upload document"/>
        <input type="button" class="classname" id="docpreview" value="Preview Document"/>
        <input type="button" class="classname" id="createfolder" value="Create Folder" />
		<input type="button" id="btnDirInfo" value="directory" class="classname"/>
      </div>
      <div align="right">
          <input type="radio" name="sex" value="simple">
          This Folder
          <input type="radio" name="sex" value="advanced">
          All Folders
        <input id ="search" type="text" name="search"  placeholder="Search"/>
      </div>
    </div>
  </div>