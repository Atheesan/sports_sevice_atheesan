<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Sports Service</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/slideshow.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/func.js"></script>
<script type="text/javascript" src="js/slidesw.js"></script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <div class="img"><img src="images/head.png" width="420" height="100" alt="" class="fl" /></div>
      </div>
      
     
   <div class="clr"></div>
      
      <div class="menu_nav">
        <ul>
          <li class="active"><a href="/CAll/index.php"><span>Home</span></a></li>
          <li><a href="/CAll/Display/Users.html"><span>Users</span></a></li>
          <li><a href="/CAll/Display/tips.html"><span>Tips</span></a></li>
          <li><a href="/CAll/Display/About_us.html"><span>About Us</span></a></li>
          <li><a href="/CAll/Display/Contact_us.html"><span>Contact Us</span></a></li>
        </ul>
      </div>
      <div class="clr"></div>
      <div class="slider">
        <div class="slider" id="coin-slider"> <a href="#"><img src="images/ar.jpg" width="935" height="272" alt="slideshow" /> </a> <a href="#"><img src="images/br.jpg" width="935" height="272" alt="slideshow" /> </a> <a href="#"><img src="images/c.jpg" width="935" height="272" alt="slideshow" /> </a> </div>
        <div class="clr"></div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Home</span></h2>
          <div class="clr"></div>
          
          <div class="post_content">
            <p>Mobile application and website will include these features:</br></br>

When someone wants to play a particular sport, he/she can log into this website and retrieve the necessary information(such as date, time slots, venue, coaches, charges, payment methods) and select on a game which overlaps with his/her requirements. Then the person can invite his/her friends who like to play that particular game on the selected date and within that time slot. Others get the chance to give their feedback and responses to such an invitation by simply logging in to this website. That's the social aspect of this web application. </br></br>


Coaches can book the courts to conduct their sessions by using this website or mobile application. They also can obtain the details of the students who are willing to come and who are already participating to their practice sessions. The application can be developed in such a manner that coaches can charge their fees through the website. In case of an emergency, if the coach had to cancel the session the participants may be getting notifications regarding that situation. These make the coach’s job easier and this is cost effective.</p>

            <p class="spec"><a href="#" class="rm">Read more &raquo;</a></p>
          </div>
      <div class="img"><img src="images/img1.jpg" width="179" height="215" alt="" class="fl" /></div>
      
      <div class="img"><img src="images/img2.jpg" width="179" height="215" alt="" class="fl" /></div>
          

        </div>
      </div>
      <div class="sidebar">
        <div class="searchform">
          <form id="formsearch" name="formsearch" method="post" action="#">
            <span>
            <input name="editbox_search" class="editbox_search" id="editbox_search" maxlength="80" value="Search our ste:" type="text" />
            </span>
            <input name="button_search" src="images/search.gif" class="button_search" type="image" />
          </form>
        </div>
        <div class="clr"></div>
        <div class="gadget">
          <h2 class="star"><span>Users</span></h2>
          <div class="clr"></div>
          <ul class="sb_menu">
         	
           <li><a href="Coach/index.php">Coach</a></li>
            <li><a href="Student/index.php">Student</a></li>
            <li><a href="Sportsman/index.php">Sportsman</a></li>
            <li><a href="Club/index.php">Club</a></li>
          </ul>
        </div>
        <div class="gadget">
          <div class="img"><img src="images/img1.jpg" width="220" height="215" alt="" class="fl" /></div>
          <div class="clr"></div>
          
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="col c1">
        <h2><span>Image</span> Gallery</h2>
        <a href="#"><img src="images/gal1.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal2.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal3.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal4.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal5.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal6.jpg" width="75" height="75" alt="" class="gal" /></a> </div>
      <div class="col c2">
        <h2><span>Our Services</h2>
        <p>We provide our best servicers to you.</p>
        <ul class="fbg_ul">
          <li><a href="#">Invite friends to play</a></li>
          <li><a href="#">Arrange tournaments</a></li>
          <li><a href="#">Find your coach</a></li>
        </ul>
      </div>
      <div class="col c3">
             <h2><span>Contact</span> Us</h2>
        <p>We are here to provide our best service to you.</p>
        <p class="contact_info">
          <span>Name : </span>Ridgecrest Asia (Pvt) Ltd.<br /> 
          <span>Address:</span>113, 5th Lane, Colombo 03.<br />
          <span>Telephone:</span>0094 11 2370876 <br />
          <span>FAX:</span>0094 11 2370878<br />
          <span>E-mail:</span> <a href="#">info@ridgecrest.lk</a> </p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="footer">
    <div class="footer_resize">
      <p class="lf">&copy; Copyright © 2013<a href="http://www.sportslk.com"> Ridgecrest, Inc. </a>All rights reserved.</p>
      <p class="rf">Design by Ridgecrest</p>
      <div style="clear:both;"></div>
    </div>
  </div>
</div>
<div class="end">
<div class="end_text">© 2013<a href="http://www.sportslk.com"> Ridgecrest, Inc. </a>All rights reserved.</div>
</body>
</html>
