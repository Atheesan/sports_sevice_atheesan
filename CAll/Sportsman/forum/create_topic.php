<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Sports Service</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/func.js"></script>
<script type="text/javascript" src="js/sport_script.js"></script>
<script type="text/javascript" src="js/slidesw.js"></script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <div class="img"><img src="images/head.png" width="420" height="100" alt="" class="fl" /></div>
      </div>
      
      <div class="reg">
        <ul>
        
        </ul>
      </div>
   <div class="clr"></div>

      <div class="menu_nav">
        <ul>
          <li><a href="index.php"><span>Home</span></a></li>
		  <li><a href="create_topic.php"><span>Create a topic</span></a></li>
        </ul>
      </div>
      <div class="clr"></div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Invite Friends to Play</span>          </h2>
          <div class="clr"></div>
          
            <p>Insert your details below and invite your friends to play with you.</br>

<?php
//create_topic.php
include 'connect.php';
include 'header.php';

echo '<h2>Create a topic</h2>';
if(isset($_SESSION["VALID_USER_ID"]) && !empty($_SESSION["VALID_USER_ID"]) && isset($_SESSION['signed_in']))
//if(!isset($_SESSION['signed_in']))
{

if(isset($_GET["page_owner"]) && !empty($_GET["page_owner"]))
	{
		$page_owner = strip_tags(base64_decode($_GET["page_owner"]));
	}
	else
	{
		$page_owner = strip_tags($_SESSION["VALID_USER_ID"]);
	}
	
	//Check the database table for the logged in user or page owner information
	$check_user_details = mysql_query("select * 
	from `friendship_system_users_table` 
	where `username` = '".mysql_real_escape_string($page_owner)."'");
	
	//Get all the logged in user or page owner information from the database users table
	$get_user_details = mysql_fetch_array($check_user_details);
	
	$user_id = strip_tags($get_user_details['id']);
	$fullname = strip_tags($get_user_details['fullname']);
	$username = strip_tags($get_user_details['username']);
	$email = strip_tags($get_user_details['email']);
	echo '<input type="hidden" id="logged_in_username" value="'.strip_tags($_SESSION["VALID_USER_ID"]).'">';
	echo '<input type="hidden" id="page_owner" value="'.$page_owner.'">';


	
	
	


	//the user is not signed in




	//the user is signed in
	if($_SERVER['REQUEST_METHOD'] != 'POST')
	{	
		//the form hasn't been posted yet, display it
		//retrieve the categories from the database for use in the dropdown
				
			
			
		
				echo '<form method="post" action="">
					Subject: <input type="text" name="topic_subject" /><br />
					Message: <br /><textarea name="post_content" /></textarea><br /><br />
					<input type="submit" value="Create topic" />
				 </form>';
		
		
	}
	else
	{
		//start the transaction
		$query  = "BEGIN WORK;";
		$result = mysql_query($query);
		
		if(!$result)
		{
			//The query failed, quit
			echo 'An error occured while creating your topic. Please try again later.';
		}
		else
		{
	
			//the form has been posted, so save it
			//insert the topic into the topics table first, then we'll save the post into the posts table
			$sql = "INSERT INTO 
						topics(topic_subject,
							   topic_date,
							   topic_by)
				   VALUES('" . mysql_real_escape_string($_POST['topic_subject']) . "',
							   NOW(), 
							   '" . $user_id . "'
							   )";
					 
			$result = mysql_query($sql);
			
			//If subject is inserted successfully inert message!
			if(!$result)
			{
				//something went wrong, display the error
				echo 'An error occured while inserting your data. Please try again later.<br /><br />' . mysql_error();
				$sql = "ROLLBACK;";
				$result = mysql_query($sql);
			}
			else
			{
				//the first query worked, now start the second, posts query
				//retrieve the id of the freshly created topic for usage in the posts query
				$topicid = mysql_insert_id();
				
				$sql = "INSERT INTO
							posts(post_content,
								  post_date,
								  post_topic,
								  post_by)
						VALUES
							('" . mysql_real_escape_string($_POST['post_content']) . "',
								  NOW(),
								  " . $topicid . ",
								  '" .  $user_id  . "'
							)";
				$result = mysql_query($sql);
				
				if(!$result)
				{
					//something went wrong, display the error
					echo 'An error occured while inserting your post. Please try again later.<br /><br />' . mysql_error();
					$sql = "ROLLBACK;";
					$result = mysql_query($sql);
				}
				else
				{
					$sql = "COMMIT;";
					$result = mysql_query($sql);
					
					//after a lot of work, the query succeeded!
					echo 'You have succesfully created <a href="topic.php?id='. $topicid . '">your new topic</a>.';
				}
			}
		}
	}
}
else
{
	echo 'Sorry, you have to be <a href="login.php">signed in</a> to create a topic.';
}
//include 'footer.php';
?>
            
         
          <div class="clr"></div>
        </div>
      </div>
      <div class="sidebar">
        <div class="searchform"></div>
 		
        <div class="img"><img src="images/logo.jpg" width="261" height="86" alt="" class="fl" /></div>
        
        <div class="clr"></div>
       <div class="gadget">
          <h2 class="star"><span>Sportsman</span></h2>
          <div class="clr"></div>
          <ul class="sb_menu">
            <li><a href="/CAll/Sports/create_sports.php">Book Court</a></li>
            <li><a href="/CAll/Sports/index.php">Invite Friends to Play</a></li>
             <li><a href="/CAll/Sportsman/forum/create_topic.php">Post on your site</a></li>
			 <li><a href="/CAll/Sports/application.php">View Notifications</a></li>
          </ul>
        </div>
        <div class="gadget">
          <h2 class="star"><span>Users</span></h2>
          <div class="clr"></div>
          <ul class="ex_menu">
            <li><a href="/CAll/Coach/index.php">Coach</a></li>
            <li><a href="/CAll/Student/index.php">Student</a></li>
            <li><a href="/CAll/Sportsman/index.php">Sportsman</a></li>
            <li><a href="/CAll/Club/index.php">Club</a></li>
          </ul>
           <div class="clr"></div>
        <div class="img"><img src="images/img1.jpg" width="220" height="215" alt="" class="fl" /></div>
        </div>
       
      </div>
      <div class="clr"></div>
    </div>
  </div>
 <div class="fbg">
    <div class="fbg_resize">
      <div class="col c1">
        <h2><span>Image</span> Gallery</h2>
        <a href="#"><img src="images/gal1.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal2.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal3.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal4.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal5.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal6.jpg" width="75" height="75" alt="" class="gal" /></a> </div>
      <div class="col c2">
         <h2><span>Our Services</h2>
        <p>We provide our best servicers to you.</p>
        <ul class="fbg_ul">
          <li><a href="#">Invite friends to play</a></li>
          <li><a href="#">Arrange tournaments</a></li>
          <li><a href="#">Find your coach</a></li>
        </ul>
      </div>
      <div class="col c3">
            <h2><span>Contact</span> Us</h2>
        <p>We are here to provide our best service to you.</p>
        <p class="contact_info">
           <span>Name : </span>Ridgecrest Asia (Pvt) Ltd.<br /> 
          <span>Address:</span>113, 5th Lane, Colombo 03.<br />
          <span>Telephone:</span>0094 11 2370876 <br />
          <span>FAX:</span>0094 11 2370878<br />
          <span>E-mail:</span> <a href="#">info@ridgecrest.lk</a> </p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="footer">
    <div class="footer_resize">
      <p class="lf">&copy; Copyright © 2013<a href="http://www.sportslk.com"> Ridgecrest, Inc. </a>All rights reserved.</p>
      <p class="rf">Design by Ridgecrest</p>
      <div style="clear:both;"></div>
    </div>
  </div>
</div>
<div class="end">
<div class="end_text">© 2013<a href="http://www.sportslk.com"> Ridgecrest, Inc. </a>All rights reserved.</div>
</body>
</html>
