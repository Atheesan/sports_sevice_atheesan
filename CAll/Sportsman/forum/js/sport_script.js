//Auto call Notification Function
$(document).ready(function()
{
	setInterval(function() { sport_notification_display(); }, 300);
});


//Notification Function
function sport_notification_display()
{
	var page_owner = $("#page_owner").val();
	var logged_in_username = $("#logged_in_username").val();
	var dataString = "logged_in_username=" + logged_in_username + "&page_owner=" + page_owner + "&page=sport_check_friends_request";
	$.ajax({  
		type: "POST",  
		url: "add_or_cancel_friend_ship.php",  
		data: dataString,
		cache: false,
		beforeSend: function() {},  
		success: function(response)
		{
			if(response == "") 
			{}
			else
			{
				$("#sport_notification_wrapper").hide().show().html(response);
			}
		}
	});
}


//Perform the Following and Unfollowing work
function add_or_cancel_friend_ship(logged_in_username,page_owner,action)
{
    var dataString = "logged_in_username=" + logged_in_username + "&page_owner=" + page_owner + "&page=" + action;
    $.ajax({  
        type: "POST",  
        url: "add_or_cancel_friend_ship.php",  
        data: dataString,
		cache: false,
        beforeSend: function() 
        {
            if ( action == "sport_add_as_friend" )
            {
                $("#sport_add_as_friend").hide();
                $("#sport_loading_friend_ship_activities").html('<img src="images/loading.gif" align="absmiddle" alt="Loading...">');
            }
            else if ( action == "sport_cancel_friendship" )
            {
				$("#sport_request_sent").html('');
				$("#sport_cancel_friendship").hide();
                $("#sport_loading_friend_ship_activities").html('<img src="images/loading.gif" align="absmiddle" alt="Loading...">');
            }
            else { }
        },  
        success: function(response)
        {
            if ( action == "sport_cancel_friendship" )
			{
                $("#sport_loading_friend_ship_activities").html('');
                $("#sport_add_as_friend").show();
				if(response == "cancelled_successfully")
				{
					$("#add_page_owner_id"+page_owner).show();
					$("#page_owner_friends_id"+logged_in_username).hide();
				}
            }
            else if ( action == "sport_add_as_friend" )
			{
                $("#sport_loading_friend_ship_activities").html('');
				$("#add_page_owner_id"+page_owner).hide();
				$("#page_owner_friends_id"+logged_in_username).show();
				
				if(response == "friend_ship_confirmed")
				{
					$("#sport_request_sent").html('');
                	$("#sport_cancel_friendship").show();
					
				}
				else if(response == "request_sent_successfully")
				{
					$("#sport_request_sent").html('<span class="sport_general_button_g" style="float:none;opacity:0.5; cursor:default;">Request Sent</span><br clear="all"><br clear="all"><br clear="all"><br clear="all">');
                	$("#sport_cancel_friendship").show();
				}
            }
            else { }
			$("#sport_notification_wrapper").hide();
        }
    }); 
}