CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(8) NOT NULL AUTO_INCREMENT,
  `post_content` text NOT NULL,
  `post_date` datetime NOT NULL,
  `post_topic` int(8) NOT NULL,
  `post_by` int(8) NOT NULL,
  PRIMARY KEY (`post_id`)
);





CREATE TABLE IF NOT EXISTS `topics` (
  `topic_id` int(8) NOT NULL AUTO_INCREMENT,
  `topic_subject` varchar(255) NOT NULL,
  `topic_date` datetime NOT NULL,
  `topic_by` int(8) NOT NULL,
  PRIMARY KEY (`topic_id`)
);




CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(8) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_date` datetime NOT NULL,
  `user_level` int(8) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name_unique` (`user_name`)
);


CREATE TABLE IF NOT EXISTS `sport` (
  `sport_id` int(8) NOT NULL AUTO_INCREMENT,
  `sport_name` varchar(255) NOT NULL,
  `sport_venue` varchar(255) NOT NULL,
  `sport_time` varchar(255) NOT NULL,
  `sport_by` int(8) NOT NULL,
  PRIMARY KEY (`sport_id`)
);



  
  ALTER TABLE `topics`
drop CONSTRAINT `topics_ibfk_1`;


ALTER TABLE `topics`
drop FOREIGN KEY `topics_ibfk_1`;



ALTER TABLE topics ADD FOREIGN KEY(topic_by) REFERENCES friendship_system_users_table(id) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE posts ADD FOREIGN KEY(post_topic) REFERENCES topics(topic_id) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE posts ADD FOREIGN KEY(post_by) REFERENCES friendship_system_users_table(id) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE sport ADD FOREIGN KEY(sport_by) REFERENCES friendship_system_users_table(id) ON DELETE RESTRICT ON UPDATE CASCADE;