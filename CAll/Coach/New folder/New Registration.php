
<!-- saved from url=(0065)https://www.linklk.com/signup/secure/Registration/New?app_key=999 -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        
        <link type="/image/x-icon" href="https://www.linklk.com/signup/resources/images/ico.gif" rel="shortcut icon">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/anytime.css">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/password.css">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/vtip.css">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/errortips.css">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/sportslk_registration.css">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/toolbar.css">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/calendar.css">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/jquery.ui.datetime.css">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/toolTips.css">
        <link rel="stylesheet" type="text/css" href="./New Registration_files/commenstyle.css">

        
        <script type="text/javascript" src="./New Registration_files/jquery.min.js"></script>
        <script type="text/javascript" src="./New Registration_files/jquery-ui.min.js"></script>
        <script type="text/javascript" src="./New Registration_files/anytime.js"></script>
        <script type="text/javascript" src="./New Registration_files/jquery.pstrength-min.1.2.js"></script>
        <script type="text/javascript" src="./New Registration_files/vtip-min.js"></script>
        <script type="text/javascript" src="./New Registration_files/errortips-min.js"></script>
        <script type="text/javascript" src="./New Registration_files/calendar.js"></script>
        <script type="text/javascript" src="./New Registration_files/validation.js"></script>
        <script type="text/javascript" src="./New Registration_files/jquery.ui.datetime.min.js"></script>
        <script type="text/javascript" src="./New Registration_files/toolTips.js"></script></head><body><div id="dhtmltooltip"></div><img id="dhtmlpointer" src="./New Registration_files/vtip_arrow.png">


        
        <script type="text/javascript">
            

            jQuery(function($) {

                function createYearArray(startY, endY){
                    var arr = new Array();
                    var endYear = parseInt(endY);
                    var startYear = parseInt(startY);
                    var i = 0;
                    for(i=startYear;i < endYear;i++){
                        arr.push(i.toString());
                    }
                    return arr;
                }

                $("#datepicker").datepicker({withTime: false, format: 'dd-mm-yyyy'});
                $("#datum1").click(function(){$("#datepicker").focus();return false;});
                $("#datepicker").change(function() {
                    var dateArr = $("#datepicker").val().split("/");
                    $("input#autocomplete1").val(dateArr[1]);
                    $("input#autocomplete2").val(dateArr[0]);
                    $("input#autocomplete3").val(dateArr[2]);
                });

                $('.password').pstrength();
                $("input#autocomplete1").autocomplete({
                    source: ["01", "02", "03", "04", "05", "06", "07","08","09","10","11",
                        "12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]
                });
                $("input#autocomplete2").autocomplete({
                    source: ["01", "02", "03", "04", "05", "06", "07","08","09","10","11","12"]
                });
            });
            
        </script>

        <style>
            .color {
                color: #ff0000;
                font-style: italic;
            }
        </style>

        <title>New Registration</title>       
    
    
        <div id="MainFrame">
            <div id="PageHeader">
                

<a href="http://www.sportslk.com/"><div class="Logo"></div></a>
<div id="control_panel">
    <ul>
        
        
            
            
                <li>
                    <a class="blue" href="http://www.sportslk.com/">
                        <img src="./New Registration_files/login.png" border="0" alt="Re">Home
                    </a>
                 </li>
            
        
    </ul>
</div>

            </div>

            <div id="PageBody">
                







<script type="text/javascript">
    window.history.go(1);
    document.title = "New Registration";
    jQuery(document).ready(function($){
        if(!false){
            $("#autocomplete1").val("DD");
            $("#autocomplete2").val("MM");
            $("#autocomplete3").val("YYYY");
            $("#mobile").val("Mobile");
            $("#phone").val("Fixed");
            $("#firstname").val("First Name");
            $("#lastname").val("Last Name");
            document.getElementById("email").focus();
        }

        $("#email").blur(function(){
            validateNewEmail($(this).attr("id"));
        });

        $("#email").focus();

        $("#hiddenCal").datepicker({
            buttonImage: '/signup/resources/images/cal_ico4.jpg',
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            showOn: 'both',
            buttonText: "Select your birth date",
            minDate: new Date((new Date()).getFullYear()-100, 1 - 1, 01),
            maxDate: new Date(),
            yearRange: ((new Date()).getFullYear()-100)+":+"+(new Date()).getFullYear(),
            dateFormat: 'yy-mm-dd',
            defaultDate: new Date(((new Date()).getFullYear()-30), 00, 01),
            beforeShow: function(input, inst){
                inst.dateSelected = false;
            },
            onSelect: function(dateText, inst){
                inst.dateSelected = true;
            },
            onClose:function(date, inst){
                if(inst.dateSelected){
                    var dates = date.split("-");
                    $("#autocomplete1").val(dates[2]);
                    $("#autocomplete2").val(dates[1]);
                    $("#autocomplete3").val(dates[0]);
                }
                var d = $("#autocomplete1").val();
                var m = $("#autocomplete2").val();
                var y = $("#autocomplete3").val();
                if((d != "") && (m != "") && (y != "") && /(\d{1,2})$/.test(d) && /(\d{1,2})$/.test(m) && /(\d{1,4})$/.test(y))
                    $("#err_autocomplete1").html("<span class='err_success'/>");
                else
                    $("#err_autocomplete1").html("<div id='DOB is invalid' "+
                    "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()' "+
                    "class='errortips'><span class='err_notification'/></div>");
            }
        });
    });
</script>
<script type="text/javascript" src="./New Registration_files/textbx-info.js"></script>
<script type="text/javascript">
    function ckeck(f) {
        f.img.value = f.image.value;
        return true;
    }   
</script>
<?php
include login.php
?>
<div id="content-mid-div">
    <div class="left seal">
        <span id="siteseal">
            <script type="text/javascript" src="./New Registration_files/getSeal"></script><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="https://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="72" height="109" id="siteseal_gd_1_v_s_dv_iv" align="middle"><param name="movie" value="https://seal.godaddy.com/flash/1/siteseal_gd_1_v_s_dv_iv.swf"><param name="quality" value="high"><param name="AllowScriptAccess" value="always"><param name="wmode" value="transparent"><embed src="https://seal.godaddy.com/flash/1/siteseal_gd_1_v_s_dv_iv.swf" wmode="transparent" quality="high" width="72" height="109" name="siteseal_gd_1_v_s_dv_iv" align="middle" type="application/x-shockwave-flash" pluginspage="https://www.macromedia.com/go/getflashplayer" allowscriptaccess="always"></object>
        </span>
    </div>
    <div id="registrationform" class="left">
        <form id="form" name="form" action="https://www.linklk.com/signup/secure/Registration/CreateNewUser" method="post" enctype="multipart/form-data">
            
			<div class="row">
                <span class="label"><label>Full Name <font color="#FF0000"> *</font></label></span>
                <span class="fieldwidth">
                <span>
                    <input id="firstname" name="fullname" onclick="if(this.value==&#39;First Name&#39;) this.value=&#39;&#39;" class="short-firstlast" type="text" value="">
                </span>
                </span>
                <span id="err_firstname"></span>
                <span class="msgContainer" id="info_firstname"></span>
            </div>
			
            <div class="row">
                <span class="label"><label>Username <font color="#FF0000"> *</font></label></span>
                <span class="fieldwidth"><input id="username" name="username" class="short-firstname" type="text" value=""></span>
                <span id="err_email"></span>
                <span class="msgContainer" id="info_username"></span>
            </div>
			
            <div class="row">
                <span class="label"><label>E-mail <font color="#FF0000"> *</font></label></span>
                <span class="fieldwidth"><input id="email" name="email" class="short-email" type="text" value=""></span>
                <span id="err_email"></span>
                <span class="msgContainer" id="info_email"></span>
            </div>
            <div class="row">
                <span class="label"><label>Password <font color="#FF0000"> *</font></label></span>
                <span class="fieldwidth"><input id="password" name="password" class="password short-pw" type="password" value=""><div style="float:left; font-size:10px; margin-left:5px;"><div class="pstrength-info" id="password_text" style="color:#CCCCCC;"></div><div class="pstrength-bar" id="password_bar" style="border: 1px solid #FFFFFF; font-size: 1px; width: 0px;"></div></div></span>
                <span id="err_password"></span>
                <span class="msgContainer" id="info_password"></span>
            </div>
            <div class="row">
                <span class="label"><label>Confirm Password <font color="#FF0000"> *</font></label></span>
                <span class="fieldwidth"><input id="confirmPassword" name="confirmPassword" class="short-pw" type="password" value=""></span>
                <span id="err_confirmPassword"></span>
                <span class="msgContainer" id="info_confirmPassword"></span>
            </div>
            
            <div class="row">
                <span class="label"><label>Gender <font color="#FF0000"> *</font></label></span>
                <span class="fieldwidth">
                    <input id="gender1" name="gender" type="radio" value="1"> Male
                    <input id="gender2" name="gender" type="radio" value="2"> Female
                </span>
                <span id="err_gender"></span>
            </div>
            <div class="row">
                <span class="label"><label>DOB <font color="#FF0000"> *</font></label></span>
                <span class="fieldwidth">
                    <input id="autocomplete1" name="date1" onclick="if(this.value==&#39;DD&#39;) {this.value=&#39;&#39;}" class="short ui-autocomplete-input" type="text" value="" size="3" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
                    <input id="autocomplete2" name="month1" onclick="if(this.value==&#39;MM&#39;) this.value=&#39;&#39;" class="short ui-autocomplete-input" type="text" value="" size="3" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
                    <input id="autocomplete3" name="year1" onclick="if(this.value==&#39;YYYY&#39;) this.value=&#39;&#39;" class="short" type="text" value="" size="6">
                    <input id="datepicker" style="display:none" class="hasDatepicker">
                    <a href="https://www.linklk.com/signup/secure/Registration/New?app_key=999#">
                        <input type="hidden" id="hiddenCal" name="hiddenCal" class="hasDatepicker"><img class="ui-datepicker-trigger" src="./New Registration_files/cal_ico4.jpg" alt="Select your birth date" title="Select your birth date">
                    </a>
                </span>
                <span id="err_autocomplete1"></span>
                <span class="msgContainer" id="info_autocomplete1"></span>
                <span class="msgContainer" id="info_autocomplete2"></span>
                <span class="msgContainer" id="info_autocomplete3"></span>
            </div>

            <div class="row">
                <span class="label"><label>NIC/Passport <font color="#FF0000"> *</font></label></span>
                <span class="fieldwidth">
                    <select id="nic" name="nic" class="NICwidth">
                        <option value="NIC">NIC</option>
                        <option value="Passport">Passport</option>
                    </select>
                    <input id="np" name="np" class="short-nic" type="text" value="" size="10">
                </span>

                <span id="err_np"></span>
                <span class="msgContainer" id="info_np"></span>
            </div>

            <div class="row">
                <span class="label"><label>Address</label></span>
                <span class="fieldwidth">
                    <textarea id="address" name="address"></textarea>
                </span>
                <span class="msgContainer" id="info_address"></span>
            </div>

            <div class="row">
                <span class="label"><label>Country <font color="#FF0000"> *</font></label></span>
                <span class="fieldwidth">
                    <select id="country" name="country" class="select">
                        <option style="width:100px" value="1">Afghanistan</option><option style="width:100px" value="2">Albania</option><option style="width:100px" value="3">Algeria</option><option style="width:100px" value="4">American Samoa</option><option style="width:100px" value="5">Andorra</option><option style="width:100px" value="6">Angola</option><option style="width:100px" value="7">Anguilla</option><option style="width:100px" value="8">Antigua and Barbuda</option><option style="width:100px" value="9">Argentina</option><option style="width:100px" value="10">Armenia</option><option style="width:100px" value="11">Aruba</option><option style="width:100px" value="12">Australia</option><option style="width:100px" value="13">Austria</option><option style="width:100px" value="14">Azerbaijan</option><option style="width:100px" value="15">Bahamas</option><option style="width:100px" value="16">Bahrain</option><option style="width:100px" value="17">Bangladesh</option><option style="width:100px" value="18">Barbados</option><option style="width:100px" value="19">Belarus</option><option style="width:100px" value="20">Belgium</option><option style="width:100px" value="21">Belize</option><option style="width:100px" value="22">Benin</option><option style="width:100px" value="23">Bermuda</option><option style="width:100px" value="24">Bhutan</option><option style="width:100px" value="25">Bolivia</option><option style="width:100px" value="26">Bosnia and Herzegovina</option><option style="width:100px" value="27">Botswana</option><option style="width:100px" value="28">Brazil</option><option style="width:100px" value="29">Brunei Darussalam</option><option style="width:100px" value="30">Bulgaria</option><option style="width:100px" value="31">Burkina Faso</option><option style="width:100px" value="32">Burundi</option><option style="width:100px" value="33">Cambodia</option><option style="width:100px" value="34">Cameroon</option><option style="width:100px" value="35">Canada</option><option style="width:100px" value="36">Cape Verde</option><option style="width:100px" value="37">Cayman Islands</option><option style="width:100px" value="38">Central African Republic</option><option style="width:100px" value="39">Chad</option><option style="width:100px" value="40">Chile</option><option style="width:100px" value="41">China</option><option style="width:100px" value="42">Colombia</option><option style="width:100px" value="43">Comoros</option><option style="width:100px" value="44">Congo</option><option style="width:100px" value="45">Congo, the Democratic Republic of the</option><option style="width:100px" value="46">Cook Islands</option><option style="width:100px" value="47">Costa Rica</option><option style="width:100px" value="48">Cote D'Ivoire</option><option style="width:100px" value="49">Croatia</option><option style="width:100px" value="50">Cuba</option><option style="width:100px" value="51">Cyprus</option><option style="width:100px" value="52">Czech Republic</option><option style="width:100px" value="53">Denmark</option><option style="width:100px" value="54">Djibouti</option><option style="width:100px" value="55">Dominica</option><option style="width:100px" value="56">Dominican Republic</option><option style="width:100px" value="57">Ecuador</option><option style="width:100px" value="58">Egypt</option><option style="width:100px" value="59">El Salvador</option><option style="width:100px" value="60">Equatorial Guinea</option><option style="width:100px" value="61">Eritrea</option><option style="width:100px" value="62">Estonia</option><option style="width:100px" value="63">Ethiopia</option><option style="width:100px" value="64">Falkland Islands (Malvinas)</option><option style="width:100px" value="65">Faroe Islands</option><option style="width:100px" value="66">Fiji</option><option style="width:100px" value="67">Finland</option><option style="width:100px" value="68">France</option><option style="width:100px" value="69">French Guiana</option><option style="width:100px" value="70">French Polynesia</option><option style="width:100px" value="71">Gabon</option><option style="width:100px" value="72">Gambia</option><option style="width:100px" value="73">Georgia</option><option style="width:100px" value="74">Germany</option><option style="width:100px" value="75">Ghana</option><option style="width:100px" value="76">Gibraltar</option><option style="width:100px" value="77">Greece</option><option style="width:100px" value="78">Greenland</option><option style="width:100px" value="79">Grenada</option><option style="width:100px" value="80">Guadeloupe</option><option style="width:100px" value="81">Guam</option><option style="width:100px" value="82">Guatemala</option><option style="width:100px" value="83">Guinea</option><option style="width:100px" value="84">Guinea-Bissau</option><option style="width:100px" value="85">Guyana</option><option style="width:100px" value="86">Haiti</option><option style="width:100px" value="87">Holy See (Vatican City State)</option><option style="width:100px" value="88">Honduras</option><option style="width:100px" value="89">Hong Kong</option><option style="width:100px" value="90">Hungary</option><option style="width:100px" value="91">Iceland</option><option style="width:100px" value="92">India</option><option style="width:100px" value="93">Indonesia</option><option style="width:100px" value="94">Iran, Islamic Republic of</option><option style="width:100px" value="95">Iraq</option><option style="width:100px" value="96">Ireland</option><option style="width:100px" value="97">Israel</option><option style="width:100px" value="98">Italy</option><option style="width:100px" value="99">Jamaica</option><option style="width:100px" value="100">Japan</option><option style="width:100px" value="101">Jordan</option><option style="width:100px" value="102">Kazakhstan</option><option style="width:100px" value="103">Kenya</option><option style="width:100px" value="104">Kiribati</option><option style="width:100px" value="105">Korea, Democratic People's Republic of</option><option style="width:100px" value="106">Korea, Republic of</option><option style="width:100px" value="107">Kuwait</option><option style="width:100px" value="108">Kyrgyzstan</option><option style="width:100px" value="109">Lao People's Democratic Republic</option><option style="width:100px" value="110">Latvia</option><option style="width:100px" value="111">Lebanon</option><option style="width:100px" value="112">Lesotho</option><option style="width:100px" value="113">Liberia</option><option style="width:100px" value="114">Libyan Arab Jamahiriya</option><option style="width:100px" value="115">Liechtenstein</option><option style="width:100px" value="116">Lithuania</option><option style="width:100px" value="117">Luxembourg</option><option style="width:100px" value="118">Macao</option><option style="width:100px" value="119">Macedonia, the Former Yugoslav Republic of</option><option style="width:100px" value="120">Madagascar</option><option style="width:100px" value="121">Malawi</option><option style="width:100px" value="122">Malaysia</option><option style="width:100px" value="123">Maldives</option><option style="width:100px" value="124">Mali</option><option style="width:100px" value="125">Malta</option><option style="width:100px" value="126">Marshall Islands</option><option style="width:100px" value="127">Martinique</option><option style="width:100px" value="128">Mauritania</option><option style="width:100px" value="129">Mauritius</option><option style="width:100px" value="130">Mexico</option><option style="width:100px" value="131">Micronesia, Federated States of</option><option style="width:100px" value="132">Moldova, Republic of</option><option style="width:100px" value="133">Monaco</option><option style="width:100px" value="134">Mongolia</option><option style="width:100px" value="135">Montserrat</option><option style="width:100px" value="136">Morocco</option><option style="width:100px" value="137">Mozambique</option><option style="width:100px" value="138">Myanmar</option><option style="width:100px" value="139">Namibia</option><option style="width:100px" value="140">Nauru</option><option style="width:100px" value="141">Nepal</option><option style="width:100px" value="142">Netherlands</option><option style="width:100px" value="143">Netherlands Antilles</option><option style="width:100px" value="144">New Caledonia</option><option style="width:100px" value="145">New Zealand</option><option style="width:100px" value="146">Nicaragua</option><option style="width:100px" value="147">Niger</option><option style="width:100px" value="148">Nigeria</option><option style="width:100px" value="149">Niue</option><option style="width:100px" value="150">Norfolk Island</option><option style="width:100px" value="151">Northern Mariana Islands</option><option style="width:100px" value="152">Norway</option><option style="width:100px" value="153">Oman</option><option style="width:100px" value="154">Pakistan</option><option style="width:100px" value="155">Palau</option><option style="width:100px" value="156">Panama</option><option style="width:100px" value="157">Papua New Guinea</option><option style="width:100px" value="158">Paraguay</option><option style="width:100px" value="159">Peru</option><option style="width:100px" value="160">Philippines</option><option style="width:100px" value="161">Pitcairn</option><option style="width:100px" value="162">Poland</option><option style="width:100px" value="163">Portugal</option><option style="width:100px" value="164">Puerto Rico</option><option style="width:100px" value="165">Qatar</option><option style="width:100px" value="166">Reunion</option><option style="width:100px" value="167">Romania</option><option style="width:100px" value="168">Russian Federation</option><option style="width:100px" value="169">Rwanda</option><option style="width:100px" value="170">Saint Helena</option><option style="width:100px" value="171">Saint Kitts and Nevis</option><option style="width:100px" value="172">Saint Lucia</option><option style="width:100px" value="173">Saint Pierre and Miquelon</option><option style="width:100px" value="174">Saint Vincent and the Grenadines</option><option style="width:100px" value="175">Samoa</option><option style="width:100px" value="176">San Marino</option><option style="width:100px" value="177">Sao Tome and Principe</option><option style="width:100px" value="178">Saudi Arabia</option><option style="width:100px" value="179">Senegal</option><option style="width:100px" value="180">Seychelles</option><option style="width:100px" value="181">Sierra Leone</option><option style="width:100px" value="182">Singapore</option><option style="width:100px" value="183">Slovakia</option><option style="width:100px" value="184">Slovenia</option><option style="width:100px" value="185">Solomon Islands</option><option style="width:100px" value="186">Somalia</option><option style="width:100px" value="187">South Africa</option><option style="width:100px" value="188">Spain</option><option style="width:100px" value="189" selected="selected">Sri Lanka</option><option style="width:100px" value="190">Sudan</option><option style="width:100px" value="191">Suriname</option><option style="width:100px" value="192">Svalbard and Jan Mayen</option><option style="width:100px" value="193">Swaziland</option><option style="width:100px" value="194">Sweden</option><option style="width:100px" value="195">Switzerland</option><option style="width:100px" value="196">Syrian Arab Republic</option><option style="width:100px" value="197">Taiwan, Province of China</option><option style="width:100px" value="198">Tajikistan</option><option style="width:100px" value="199">Tanzania, United Republic of</option><option style="width:100px" value="200">Thailand</option><option style="width:100px" value="201">Togo</option><option style="width:100px" value="202">Tokelau</option><option style="width:100px" value="203">Tonga</option><option style="width:100px" value="204">Trinidad and Tobago</option><option style="width:100px" value="205">Tunisia</option><option style="width:100px" value="206">Turkey</option><option style="width:100px" value="207">Turkmenistan</option><option style="width:100px" value="208">Turks and Caicos Islands</option><option style="width:100px" value="209">Tuvalu</option><option style="width:100px" value="210">Uganda</option><option style="width:100px" value="211">Ukraine</option><option style="width:100px" value="212">United Arab Emirates</option><option style="width:100px" value="213">United Kingdom</option><option style="width:100px" value="214">United States</option><option style="width:100px" value="215">Uruguay</option><option style="width:100px" value="216">Uzbekistan</option><option style="width:100px" value="217">Vanuatu</option><option style="width:100px" value="218">Venezuela</option><option style="width:100px" value="219">Viet Nam</option><option style="width:100px" value="220">Virgin Islands, British</option><option style="width:100px" value="221">Virgin Islands, U.s.</option><option style="width:100px" value="222">Wallis and Futuna</option><option style="width:100px" value="223">Western Sahara</option><option style="width:100px" value="224">Yemen</option><option style="width:100px" value="225">Zambia</option><option style="width:100px" value="226">Zimbabwe</option>
                    </select>
                </span>
                <span class="msgContainer" id="info_country"></span>
            </div>
            <div class="row">
                <span class="label"><label>Phone Number <font color="#FF0000"> *</font></label></span>
                <span class="fieldwidth">
                    <select id="countrycode" name="countrycode" style="width:55px !important;" class="short-code">
                        <option value="192">+0</option><option value="35">+1</option><option value="164">+1</option><option value="214">+1</option><option value="102">+7</option><option value="168">+7</option><option value="58">+20</option><option value="187">+27</option><option value="77">+30</option><option value="142">+31</option><option value="20">+32</option><option value="68">+33</option><option value="188">+34</option><option value="90">+36</option><option value="87">+39</option><option value="98">+39</option><option value="167">+40</option><option value="195">+41</option><option value="13">+43</option><option value="213">+44</option><option value="53">+45</option><option value="194">+46</option><option value="152">+47</option><option value="162">+48</option><option value="74">+49</option><option value="159">+51</option><option value="130">+52</option><option value="50">+53</option><option value="9">+54</option><option value="28">+55</option><option value="40">+56</option><option value="42">+57</option><option value="218">+58</option><option value="122">+60</option><option value="12">+61</option><option value="93">+62</option><option value="160">+63</option><option value="145">+64</option><option value="182">+65</option><option value="200">+66</option><option value="100">+81</option><option value="219">+84</option><option value="41">+86</option><option value="206">+90</option><option value="92">+91</option><option value="154">+92</option><option value="220">+92</option><option value="1">+93</option><option value="189" selected="selected">+94</option><option value="138">+95</option><option value="94">+98</option><option value="136">+212</option><option value="3">+213</option><option value="205">+216</option><option value="114">+218</option><option value="72">+220</option><option value="179">+221</option><option value="128">+222</option><option value="124">+223</option><option value="83">+224</option><option value="31">+226</option><option value="147">+227</option><option value="201">+228</option><option value="22">+229</option><option value="129">+230</option><option value="113">+231</option><option value="181">+232</option><option value="75">+233</option><option value="148">+234</option><option value="39">+235</option><option value="38">+236</option><option value="34">+237</option><option value="36">+238</option><option value="177">+239</option><option value="60">+240</option><option value="71">+241</option><option value="44">+243</option><option value="45">+243</option><option value="6">+244</option><option value="84">+245</option><option value="180">+248</option><option value="190">+249</option><option value="169">+250</option><option value="63">+251</option><option value="186">+252</option><option value="54">+253</option><option value="69">+254</option><option value="103">+254</option><option value="199">+255</option><option value="210">+256</option><option value="32">+257</option><option value="137">+258</option><option value="225">+260</option><option value="120">+261</option><option value="226">+263</option><option value="139">+264</option><option value="121">+265</option><option value="112">+266</option><option value="27">+267</option><option value="193">+268</option><option value="43">+269</option><option value="170">+290</option><option value="61">+291</option><option value="11">+297</option><option value="65">+298</option><option value="78">+299</option><option value="80">+312</option><option value="76">+350</option><option value="163">+351</option><option value="117">+352</option><option value="96">+353</option><option value="91">+354</option><option value="2">+355</option><option value="125">+356</option><option value="51">+357</option><option value="67">+358</option><option value="30">+359</option><option value="116">+370</option><option value="110">+371</option><option value="62">+372</option><option value="132">+373</option><option value="10">+374</option><option value="19">+375</option><option value="5">+376</option><option value="133">+377</option><option value="176">+378</option><option value="211">+380</option><option value="48">+384</option><option value="49">+385</option><option value="184">+386</option><option value="26">+387</option><option value="119">+389</option><option value="105">+408</option><option value="106">+410</option><option value="107">+414</option><option value="52">+420</option><option value="183">+421</option><option value="115">+423</option><option value="127">+474</option><option value="64">+500</option><option value="21">+501</option><option value="82">+502</option><option value="59">+503</option><option value="88">+504</option><option value="146">+505</option><option value="47">+506</option><option value="156">+507</option><option value="173">+508</option><option value="86">+509</option><option value="25">+591</option><option value="85">+592</option><option value="57">+593</option><option value="158">+595</option><option value="191">+597</option><option value="215">+598</option><option value="143">+599</option><option value="166">+638</option><option value="150">+672</option><option value="29">+673</option><option value="140">+674</option><option value="157">+675</option><option value="203">+676</option><option value="185">+677</option><option value="217">+678</option><option value="66">+679</option><option value="155">+680</option><option value="222">+681</option><option value="46">+682</option><option value="149">+683</option><option value="175">+685</option><option value="104">+686</option><option value="144">+687</option><option value="209">+688</option><option value="70">+689</option><option value="202">+690</option><option value="131">+691</option><option value="126">+692</option><option value="223">+732</option><option value="221">+850</option><option value="89">+852</option><option value="118">+853</option><option value="33">+855</option><option value="109">+856</option><option value="161">+870</option><option value="17">+880</option><option value="197">+886</option><option value="123">+960</option><option value="111">+961</option><option value="101">+962</option><option value="196">+963</option><option value="95">+964</option><option value="178">+966</option><option value="224">+967</option><option value="153">+968</option><option value="212">+971</option><option value="97">+972</option><option value="16">+973</option><option value="165">+974</option><option value="24">+975</option><option value="134">+976</option><option value="141">+977</option><option value="198">+992</option><option value="207">+993</option><option value="14">+994</option><option value="73">+995</option><option value="108">+996</option><option value="216">+998</option><option value="15">+1242</option><option value="18">+1246</option><option value="7">+1264</option><option value="8">+1268</option><option value="37">+1345</option><option value="23">+1441</option><option value="79">+1473</option><option value="208">+1649</option><option value="135">+1664</option><option value="151">+1670</option><option value="81">+1671</option><option value="4">+1684</option><option value="172">+1758</option><option value="55">+1767</option><option value="174">+1784</option><option value="56">+1809</option><option value="204">+1868</option><option value="171">+1869</option><option value="99">+1876</option>
                    </select>
                    <input id="mobile" name="mobile" onclick="if(this.value==&#39;Mobile&#39;) this.value=&#39;&#39;" class="short-mobi" type="text" value="Mobile" size="8">
                    <input id="phone" name="phone" onclick="if(this.value==&#39;Fixed&#39;) {this.value=&#39;&#39;}" class="short-lan" type="text" value="" size="8">
                </span>
                <span id="err_mobile"></span>
                <span class="msgContainer" id="info_mobile"></span>
                <span class="msgContainer" id="info_phone"></span>
            </div>
            <div class="row">
                <span class="label">
                    <label for="fileData">Photo</label>
                </span>
                <span class="fieldwidth">
                    <div class="BrowseBtnFrameTop">
                        <input id="image" name="fileData" type="file" onchange="ckeck(this.form);" value="">
                        (Max 1MB - .gif, jpg, jpeg, png)
                        <div class="BrowseBtnFrame">
                            <div class="BrowseBtnCenter">
                                <input type="text" name="img" id="img" class="BrowseBtnText short-pic">
                                <img alt="Browse" class="BrowseBtn" src="./New Registration_files/0.png" border="0">
                            </div>
                        </div>
                    </div>
                </span>
                <span id="err_fileData">  
                    
                </span>
                <span class="msgContainer" id="info_image"></span>
            </div>
            
            <div class="row">
                <span class="fieldmargin"><img alt="" class="imgcapture" id="captcha" src="./New Registration_files/captcha"></span>
            </div>
            
            <div class="row">
                <span><input id="appkey" name="appkey" value="999" type="hidden"></span>
                <span class="fieldmarginbtn">
                    <div class="left"><input type="submit" value="Submit" class="button" id="err"></div>
                    <div class="left"><input type="reset" value="Reset" class="button"></div>
                   
                </span>
            </div>
        </form>
    </div>
</div>
            </div>

            <div class="clear"></div>

            <div id="PageFooter">
                
<div class="link">
    <center>
        <a href="https://www.linklk.com/signup/Help/PrivacyPolicy" target="_blank">Privacy Policy</a> |
        <a href="https://www.linklk.com/signup/Help/Disclaimer" target="_blank">Disclaimer</a> |
        <a href="https://www.linklk.com/signup/Help/TermsOfUse" target="_blank">Terms of Use</a> |
    </center>
</div>
<div class="FooterContent">
    <div class="copyright"> Copyright � 2009 - 2013 Ridgecrest, Inc. All rights reserved.</div>
</div>
               
            </div>
        </div>
     
    



<div id="ui-datepicker-div" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div><ul class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all" role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1; top: 0px; left: 0px; display: none;"></ul><ul class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all" role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1; top: 0px; left: 0px; display: none;"></ul></body></html>