/**
Vertigo Tip by www.vertigo-project.com
Requires jQuery
*/
this.errortips=function(){
    this.xOffset=-10;
    this.yOffset=10;
    $(".errortips").unbind().hover(function(a){
        this.t=this.title;
        this.title="";
        this.top=(a.pageY+yOffset);
        this.left=(a.pageX+xOffset);
        $("body").append('<p id="errortips"><img id="vtipArrow" />'+this.t+"</p>");
        $("p#errortips #vtipArrow").attr("src","/signup/resources/images/vtip_arrow.png");
        $("p#errortips").css("top",this.top+"px").css("left",this.left+"px").fadeIn("slow")
        },function(){
        this.title=this.t;
        $("p#errortips").fadeOut("slow").remove()
        }).mousemove(function(a){
        this.top=(a.pageY+yOffset);
        this.left=(a.pageX+xOffset);
        $("p#errortips").css("top",this.top+"px").css("left",this.left+"px")
        })
    };

jQuery(document).ready(function(a){
    errortips()
    });
