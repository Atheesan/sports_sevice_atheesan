
function validateNewEmail(id){
    $('#info_'+id).html("");
    var email=$('#email').val();
    $("#email").val(email.allTrim());
    email=$('#email').val();
    $.ajax({
        type: "POST",
        url: "/signup/ValidationServices",
        data: {
            email:email,
            element_id:id
        },
        success: function(data, textStatus, jqXHR) {
            $('#err_'+id).html(jqXHR.responseText);
        }
    });
};

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
String.prototype.allTrim = function() {
    return this.replace(/\s+/g,"");
}

function isACorrectEmailFormat(elementValue){
    elementValue = elementValue.allTrim();
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(elementValue);
}

function validateEditEmail(id){
    $('#info_'+id).html("");
    var email = $('#email');
    if(email.length == 1){
        email=$('#email').val();
        $("#email").val(email.allTrim());
    }
    email=$('#email').val();
    if(isACorrectEmailFormat(email)){
        if(!(existingEmail.trim() == email.trim())){
            $.ajax({
                type: "POST",
                url: "/signup/ValidationServices",
                data: {
                    email:email,
                    element_id:id
                },
                success: function(data, textStatus, jqXHR) {
                    $('#err_'+id).html(jqXHR.responseText);
                }
            });
        }
    }else{
        $('#err_email').html("<div id='Email is not a valid email address.' "+
            "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'"+
            " class='errortips'><span class='err_notification'/></div>");
    }
};

function info(msg,id){
    var message = "<div class='infomsg'>"+msg+"</div>";
    $('#info_'+id).html(message);
};

function info_pass(msg,id){
    var message="<div class='infomsg'>"+msg+"</div>";
    $('#info_'+id).html(message);
    $('#err_confirmPassword').html("");
};

function exitInfo(id){
    $('#info_'+id).html("");
    var dd =  $('#autocomplete1').val();
    var mm =  $('#autocomplete2').val();
    var yy =  $('#autocomplete3').val();
       
    if(dd==""){
        $('#autocomplete1').val("DD");
    }
    if(mm==""){
        $('#autocomplete2').val("MM");
    }
    if(yy==""){
        $('#autocomplete3').val("YYYY");
    }
};

function exitCode(id){
    $('#info_'+id).html("");
    var a = $('#code').val();
    var a1 = a.toUpperCase();
    $('#code').val(a1);
}

function checkLength(id) {
    $('#info_'+id).html("");
    $('#err_'+id).html("");
    if ($("#"+id).val().length < 6 && $("#"+id).val() !="") {
        $('#err_'+id).html(
            "<div id='Password Length must be at-least 6 Characters.' "+
            "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()' "+
            "class='errortips'><span class='err_notification'/></div>");
    }
    
    if(!($('#password').val() == $('#confirmPassword').val())){
        $('#err_password').html(
            "<div id='Password and Confirm Password is not matching.'"+
            "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'"+
            " class='errortips'><span class='err_notification'/></div>");
        $('#err_confirmPassword').html(
            "<div id='Password and Confirm Password is not matching.' "+
            "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'"+
            " class='errortips'><span class='err_notification'/></div>");
    }
    if(($('#password').val() == $('#confirmPassword').val()) && ($('#password').val().length >=6) && ($('#confirmPassword').val().length >= 6)){
        $('#err_password').html("<span class='err_success'/>");
        $('#err_confirmPassword').html("<span class='err_success'/>");
    }
    if ($("#"+id).val().length >= 6 && $("#"+id).val() !="") {
        $('#err_password').html("<span class='err_success'/>");
    }
    if($('#password').val() ==""){
        $('#err_password').html("");
    }
}

function charOnly(id){
    $('#info_'+id).html("");
    $('#err_'+id).html("");
    var a = $('#firstname').val();
    var b = $('#lastname').val();

    if(a ==""){
        $('#firstname').val("First Name");
    }
    if(b==""){
        $('#lastname').val("Last Name");
    }
    var expr = /^[a-zA-Z][a-zA-Z\s\\.]{0,}[a-zA-Z\\.]*$/;
    var fname = $('#firstname').val();
    var lname = $('#lastname').val();
    
    if((fname!=""&& lname!="")){
        if(!fname.match(expr)){
            $('#err_firstname').html("<div id='Only Characters are Allowed.' "+
                "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'"+
                " class='errortips'><span class='err_notification'/></div>");
        }

        if(!lname.match(expr)){
            $('#err_firstname').html("<div id='Only Characters are Allowed.'"+
                "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'"+
                " class='errortips'><span class='err_notification'/></div>");
        }

        if((fname!=""&& lname!="") && (fname.match(expr) && lname.match(expr) && fname!="First Name" && lname!="Last Name")){
            $('#err_firstname').html("<span class='err_success'/>");
        }


        if((fname!=""&& lname!="") && (!fname.match(expr) || !lname.match(expr) && fname!="First Name" && lname!="Last Name")){
            $('#err_firstname').html("<div id='Only Characters are Allowed.'"+
                "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'"+
                " class='errortips'><span class='err_notification'/></div>");
        }
        
        if(fname.length < 2 || fname.length > 20){
           $('#err_firstname').html("<div id='First Name must be between 2- 20 characters.' "+
                "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'"+
                " class='errortips'><span class='err_notification'/></div>");
        }
        if(lname.length < 2 || lname.length > 20){
           $('#err_firstname').html("<div id='Last Name must be between 2- 20 characters.' "+
                "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'"+
                " class='errortips'><span class='err_notification'/></div>");
        }
    }
    
    if(fname!=""&& fname!="First Name" && lname!="" && lname=="Last Name" && fname.match(expr)){
        $('#err_firstname').html("<div id='Last Name Required' "+
            "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()' class='errortips'><span class='err_notification'/></div>");
    }

    if(lname!=""&& lname!="Last Name" && fname!="" && fname=="First Name" && lname.match(expr)){
        $('#err_firstname').html("<div id='First Name Required'"+
            "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()' class='errortips'><span class='err_notification'/></div>");
    }
}

jQuery(function($){
    $("#np").blur(function(e){
        var id = $(e.target).attr("id");
        $('#info_'+id).html("");
        var a=$('#np').val();
        var a1 =a.toString().toUpperCase();
        $('#np').val(a1);
    });

    function dobHoldValidFormat(){
        var d = $("input#autocomplete1").val();
        var m = $("input#autocomplete2").val();
        var y = $("input#autocomplete3").val();
        return /(\d{1,2})$/.test(d) && /(\d{1,2})$/.test(m) && /(\d{4})$/.test(y);
    }

    function stripLeadingZero(number){
        return number.replace(/^0/,"");
    }

    $("input:radio").click(function(){
        $("#err_gender").html("<span class='err_success'/>");
    });

    $(":reset").click(function(){
        $('div. span')
        .filter(function() {
            return this.id.match(/^err_*/);
        }).html("");
    });

    function nicBobServerValidate(){
        var d = $("input#autocomplete1").val();
        var m = $("input#autocomplete2").val();
        var y = $("input#autocomplete3").val();
        $.ajax({
            type: "POST",
            url: "/signup/ValidationServices/MatchDoBWithNic",
            data: "dobY="+y+"&dobM="+m+"&dobD="+d+"&nic="+$("#np").val(),
            success: function(data, textStatus, jqXHR) {
                (jqXHR.responseText == "true") ? $("#err_np").html("") :
                $("#err_np").html("<div id='NIC DOB mismatch' "+
                    "class='errortips'><span class='err_notification'/></div>");
            }
        });
    };

    function setDobInputStatusMsgs(){
        var nicOptionSelected = $("#nic").val() == "NIC" ? true : false;
        var nicOrPasspt = $("#np").val();
        var dobSelected = dobHoldValidFormat();
        !dobSelected ? $("#err_autocomplete1").html("<div id='DOB is invalid' onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()' "+
            "class='errortips'><span class='err_notification'/></div>") : $("#err_autocomplete1").html("<span class='err_success'/>");

        if(nicOptionSelected){
            if(/^[0-9]{9}(V|X|v|x)$/.test(nicOrPasspt)){
                $("#err_np").html("<span class='err_success'/>");
                if(dobSelected){
                    nicBobServerValidate();
                }else{
                    //DOB nic mismatch
                    $("#err_np").html("<div id='' "+
                        "class='errortips'><span class='err_notification'/></div>");
                }
            }else{
                $("#err_np").html("<div id='Invalid NIC format' "+
                    "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()' class='errortips'><span class='err_notification'/></div>");
            }
        }else{
            if(nicOrPasspt == ""){
                $("#err_np").html("<div id='Passport cannot be blank' "+
                    "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()' "+
                    "class='errortips'><span class='err_notification'/></div>");
            }else{
                $("#err_np").html("<span class='err_success'/>");
            }
        }
    };

    $("input#autocomplete1, input#autocomplete2, input#autocomplete3").change(setDobInputStatusMsgs);
    $("#np, #nic").blur(setDobInputStatusMsgs);
    $('option', $("#countrycode")).each(function(){
        $(this).text("+"+$(this).text());
    });
    $("#country").change(function(){
        $("#countrycode").val($(this).find("option:selected").val());
    });

    function autoCheckOnAlerts(eId){
        $("#"+eId).attr('checked','checked');
    }
    function autoUncheckAlerts(eId){
        $("#"+eId).removeAttr('checked');
    }

    function addLeadingZeroToDateAndMonth(){
        $("#autocomplete1, #autocomplete2").each(function(){
            if($(this).val().length == 1){
                var oneDigit = $(this).val();
                $(this).val("0"+oneDigit);
            }
        })
    }
    
    addLeadingZeroToDateAndMonth();
    var email = $("#email");
    if(email.length == 1){
        email = email.val();
        $("#email").val(email.allTrim());
    }
            
    $("#mobile, #phone").blur(function(e){
        var tMb = stripLeadingZero($("#mobile").val());
        var tFMb = stripLeadingZero($("#phone").val());
        $("#mobile").val(tMb);
        $("#phone").val(tFMb);
        var id = $(e.target).attr("id");
        $('#info_'+id).html("");
        var expr = /^[0-9]+$/;
        var expr1 = /^[7][1|2|5|7|8][0-9]{7}$/;
        var expr2 = /^[0-9]{9}/;
        var mobile = $('#mobile').val();
        var phone = $('#phone').val();

        if(($('#countrycode option:selected').text().match(+94))){
            if((mobile!=""&& phone!="" && (phone=="Fixed"||mobile=="Mobile"))){
                if(phone=="Fixed"){
                    if((mobile.match(expr))){
                        if(mobile.match(expr1)){
                            $('#err_mobile').html("<span class='err_success'/>");
                        }else{
                            $('#err_mobile').html("<div id='Invalid Mobile Number' class='errortips' onMouseover='ddrivetip(this.id)'"+
                                " onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                        }
                    }else{
                        $('#err_mobile').html("<div id='Only Numbers are Allowed.' class='errortips' onMouseover='ddrivetip(this.id)' "+
                            " onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                    }
                }
                if(mobile=="Mobile"){
                    if((phone.match(expr))){
                        if(phone.match(expr2)&&phone.length==9){
                            $('#err_mobile').html("<span class='err_success'/>");
                        }else {
                            $('#err_mobile').html("<div id='Fixed Line must contain 9 digits.' "+
                                " class='errortips' onMouseover='ddrivetip(this.id)' "+
                                "onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                        }
                    }else{
                        $('#err_mobile').html("<div id='Only Numbers are Allowed.' "+
                            "class='errortips' onMouseover='ddrivetip(this.id)' "+
                            "onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                        autoUncheckAlerts('sms');
                    }
                }
            }

            if(((mobile!=""&& phone!="") && (phone!="Fixed"&&mobile!="Mobile"))){
                if((mobile.match(expr) && phone.match(expr))){
                    if(mobile.match(expr1)){
                        if(phone.match(expr2)&&phone.length==9){
                            $('#err_mobile').html("<span class='err_success'/>");
                        }
                        else {
                            $('#err_mobile').html("<div id='Fixed Line must contain 9 digits.' "+
                                "class='errortips' onMouseover='ddrivetip(this.id)' "+
                                "onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                        }
                    }else{
                        $('#err_mobile').html("<div id='Invalid Mobile Number' class='errortips' "+
                            " onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                        autoUncheckAlerts('sms');
                    }
                }else{
                    $('#err_mobile').html("<div id='Only Numbers are Allowed.' class='errortips' "+
                        "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                }
            }

            if(((mobile==""&& phone!="") && (phone!="Fixed"))){
                if((phone.match(expr))){
                    if(phone.match(expr2)&&phone.length==9){
                        $('#err_mobile').html("<span class='err_success'/>");
                    }
                    else {
                        $('#err_mobile').html("<div id='Fixed Line must contain 9 digits.' "+
                            " class='errortips' onMouseover='ddrivetip(this.id)' "+
                            "onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                    }

                }else{
                    $('#err_mobile').html("<div id='Only Numbers are Allowed.' class='errortips' "+
                        "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                }
            }

            if(((mobile!=""&& phone=="") && (mobile!="Mobile"))){
                if(mobile.match(expr)){
                    if(mobile.match(expr1)){
                        $('#err_mobile').html("<span class='err_success'/>");
                        autoCheckOnAlerts('sms');
                    }else{
                        $('#err_mobile').html("<div id='Invalid Mobile Number' class='errortips'"+
                            " onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                        autoUncheckAlerts('sms');
                    }

                }else{
                    $('#err_mobile').html("<div id='Only Numbers are Allowed.' class='errortips' "+
                        "onMouseover='ddrivetip(this.id)' onMouseout='hideddrivetip()'><span class='err_notification'/></div>");
                }
            }


        }else{

        }
        var mob = $('#mobile').val();
        var phn = $('#phone').val();
        if(mob ==""){
            $('#mobile').val("Mobile");
        }
        if(phn==""){
            $('#phone').val("Fixed");
        }
    });

    $("input#password, input#confirmPassword").blur(function(){
        checkLength($(this).attr("id"));
    });

    $("#firstname, #lastname").blur(function(){
        charOnly($(this).attr("id"));
    });

    $("#date, #month, #year, #country, #address, #image, #news, #sms, #code, #err").blur(function(){
        exitInfo($(this).attr("id"));
    });

    $("#code, #gender1, #gender2, #np, #phone, #mobile").focus(function(){
        $("#err_"+$(this).attr("id")).html("");
    });
});