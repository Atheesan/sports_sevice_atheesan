$(document).ready(function(){
    $("#email").focus(function(){
        info('Please enter a valid email address in order to receive your account activation email. '+
            'We will not spam or disclose your email address to anyone for any reason.', $(this).attr("id"));
    });

    $("#password").focus(function(){
        info('Password length should be at least 6 characters. Use a combination of letters, '+
            'characters & special characters to make it more secure.', $(this).attr("id"));
    });

    $("#confirmPassword").focus(function(){
        info_pass('Please re-enter the password to confirm.', $(this).attr("id"));
    });

    $("#firstname").focus(function(){
        if(this.value=='First Name' || this.value=='') {
            info('Enter Your First Name & Last Name here. Only characters are allowed.', $(this).attr("id"));
            this.value='';
        };
        if(this.value !=''){
            info('Enter Your First Name & Last Name here. Only characters are allowed.', $(this).attr("id"));
        };
    });

    $("#lastname").focus(function(){
        if(this.value=='Last Name'|| this.value=='') {
            info('Enter Your First Name & Last Name here. Only characters are allowed.', $(this).attr("id"));
            this.value='';
        };
        if(this.value !=''){
            info('Enter Your First Name & Last Name here. Only characters are allowed.', $(this).attr("id"));
        };
    });

    $("#date").focus(function(){
        if(this.value=='DD' || this.value =='') {
            info('Date of Birth should be in DD MM YYYY format.', $(this).attr("id"));
            this.value='';
        };
        if(this.value !=''){
            info('Date of Birth should be in DD MM YYYY format.', $(this).attr("id"));
        };
    });

    $("#month").focus(function(){
        if(this.value=='MM'|| this.value =='') {
            info('Date of Birth should be in DD MM YYYY format.', $(this).attr("id"));
            this.value='';
        };
        if(this.value !=''){
            info('Date of Birth should be in DD MM YYYY format.', $(this).attr("id"));
        };
    });

    $("#year").focus(function(){
        if(this.value=='YYYY'|| this.value =='') {
            info('Date of Birth should be in DD MM YYYY format.', $(this).attr("id"));
            this.value='';
        };
        if(this.value !=''){
            info('Date of Birth should be in DD MM YYYY format.', $(this).attr("id"));
        };
    });

    $("#np").focus(function(){
        info('Please enter your NIC or passport number.(NIC format :- 812345678V).', $(this).attr("id"));
    });

    $("#address").focus(function(){
        info('Enter your address here.', $(this).attr("id"));
    });

    $("#country").focus(function(){
        info('Select your country.', $(this).attr("id"));
    });

    $("#mobile").focus(function(){
        if(this.value=='Mobile'|| this.value =='') {
            info('Enter your mobile number in order to recieve the account activation '+
                'code(Mobile Number format :- 711234567).', $(this).attr("id"));
            this.value='';
        };
        if(this.value !=''){
            info('Enter your mobile number in order to recieve the account activation '+
                'code(Mobile Number format :- 711234567).', $(this).attr("id"));
        };
    });

    $("#phone").focus(function(){
        if(this.value=='Fixed'|| this.value =='') {
            info('Enter your Fixed Line here (Fixed Line Number format :- 112345678).', $(this).attr("id"));
            this.value='';
        };
        if(this.value !=''){
            info('Enter your Fixed Line here (Fixed Line Number format :- 112345678).', $(this).attr("id"));
        };
    });

    $("#image").focus(function(){
        info('Upload your profile picture.Image type must be either jpg,png or '+
            'gif & file size must be less than 1Mb', $(this).attr("id"));
    });

    $("#news").focus(function(){
        info('Please confirm for newsletter subscription.', $(this).attr("id"));
    });

    $("#sms").focus(function(){
        info('Please confirm for sms alerts on ticket booking information.', $(this).attr("id"));
    });

    $("#code").focus(function(){
        info('Type the characters you see in the picture above.', $(this).attr("id"));
    });

    $("#nic").change(function(){
        $("#np").val("");
    });

    $("#backToClientApp").click(function(){
        location.href = $("#PageHeader a").attr("href");
        return false;
    });

    function formatDM(){
        var cVal = $(this).val();
        if(/^[1-9]{1}$/.test(cVal)){
            $(this).val("0"+cVal);
        }
    }
    $("#autocomplete1, #autocomplete2").blur(formatDM).change(formatDM);
});