-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 25, 2013 at 03:18 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `coach_student`
--

-- --------------------------------------------------------

--
-- Table structure for table `friendship_system_users_table`
--

CREATE TABLE IF NOT EXISTS `friendship_system_users_table` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(200) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `friendship_system_users_table`
--

INSERT INTO `friendship_system_users_table` (`id`, `fullname`, `username`, `email`, `password`, `date`) VALUES
(1, 'Athi', 'a', 'a@gmail.com', '0cc175b9c0f1b6a831c399e269772661', '03-08-2013'),
(3, 'Q', 'q', 'q@gmail.com', '7694f4a66316e53c8cdd9d9954bd611d', '05-08-2013'),
(4, 'W', 'w', 'w@gmail.com', 'f1290186a5d0b1ceab27f4e77c0c5d68', '06-08-2013'),
(5, 'S', 's', 's@gmail.com', '03c7c0ace395d80182db07ae2c30f034', '06-08-2013'),
(6, 'Zzz', 'zz', 'z@z.com', '25ed1bcb423b0b7200f485fc5ff71c8e', '19-08-2013'),
(7, 'Ddd', 'd', 'd@d.com', '8277e0910d750195b448797616e091ad', '12-09-2013'),
(8, 'Y', 'y', 'y@y.com', '415290769594460e2e485922904f345d', '21-10-2013'),
(9, 'C', 'c', 'c@c.com', '4a8a08f09d37b73795649038408b5f33', '23-10-2013');

-- --------------------------------------------------------

--
-- Table structure for table `friend_request`
--

CREATE TABLE IF NOT EXISTS `friend_request` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `friend` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=104 ;

--
-- Dumping data for table `friend_request`
--

INSERT INTO `friend_request` (`id`, `username`, `friend`) VALUES
(97, 'q', 's'),
(103, 's', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `my_friends`
--

CREATE TABLE IF NOT EXISTS `my_friends` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `friend` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `my_friends`
--

INSERT INTO `my_friends` (`id`, `username`, `friend`) VALUES
(30, 'q', 'zz'),
(31, 'zz', 'q'),
(36, 'zz', 'd'),
(37, 'd', 'zz'),
(56, 'a', 'q'),
(57, 'q', 'a'),
(58, 'w', 'd'),
(59, 'd', 'w'),
(60, 'w', 'q'),
(61, 'q', 'w'),
(62, 'y', 'q'),
(63, 'q', 'y'),
(64, 'y', 'a'),
(65, 'a', 'y'),
(66, 's', 'w'),
(67, 'w', 's'),
(68, 'zz', 'a'),
(69, 'a', 'zz');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(8) NOT NULL AUTO_INCREMENT,
  `post_content` text NOT NULL,
  `post_date` datetime NOT NULL,
  `post_topic` int(8) NOT NULL,
  `post_by` varchar(255) NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `post_topic` (`post_topic`),
  KEY `post_by` (`post_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `topic_id` int(8) NOT NULL AUTO_INCREMENT,
  `topic_subject` varchar(255) NOT NULL,
  `topic_date` datetime NOT NULL,
  `topic_by` varchar(255) NOT NULL,
  PRIMARY KEY (`topic_id`),
  KEY `topic_by` (`topic_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`post_topic`) REFERENCES `topics` (`topic_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`post_topic`) REFERENCES `topics` (`topic_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_ibfk_3` FOREIGN KEY (`post_by`) REFERENCES `friendship_system_users_table` (`username`),
  ADD CONSTRAINT `posts_ibfk_4` FOREIGN KEY (`post_topic`) REFERENCES `topics` (`topic_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_ibfk_5` FOREIGN KEY (`post_topic`) REFERENCES `topics` (`topic_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_ibfk_6` FOREIGN KEY (`post_by`) REFERENCES `friendship_system_users_table` (`username`);

--
-- Constraints for table `topics`
--
ALTER TABLE `topics`
  ADD CONSTRAINT `topics_ibfk_1` FOREIGN KEY (`topic_by`) REFERENCES `friendship_system_users_table` (`username`),
  ADD CONSTRAINT `topics_ibfk_2` FOREIGN KEY (`topic_by`) REFERENCES `friendship_system_users_table` (`username`),
  ADD CONSTRAINT `topics_ibfk_3` FOREIGN KEY (`topic_by`) REFERENCES `friendship_system_users_table` (`username`),
  ADD CONSTRAINT `topics_ibfk_4` FOREIGN KEY (`topic_by`) REFERENCES `friendship_system_users_table` (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
