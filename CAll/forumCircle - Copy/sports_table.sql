CREATE TABLE IF NOT EXISTS `sport` (
  `sport_id` int(8) NOT NULL AUTO_INCREMENT,
  `sport_name` varchar(255) NOT NULL,
  `sport_venue` varchar(255) NOT NULL,
  `sport_time` varchar(255) NOT NULL,
  `sport_date` varchar(255) NOT NULL,
  `sport_by` int(8) NOT NULL,
  PRIMARY KEY (`sport_id`)
);

ALTER TABLE sport ADD FOREIGN KEY(sport_by) REFERENCES friendship_system_users_table(id) ON DELETE RESTRICT ON UPDATE CASCADE;