<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Sports Service</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/func.js"></script>
<script type="text/javascript" src="js/slidesw.js"></script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <div class="img"><img src="images/head.png" width="420" height="100" alt="" class="fl" /></div>
      </div>
      
      <div class="reg">
        <ul>
         
        </ul>
      </div>
   <div class="clr"></div>

      <div class="menu_nav">
        <ul>
         <li><a href="index.php"><span>Home</span></a></li>
		  <li><a href="create_topic.php"><span>Create a topic</span></a></li>		
        </ul>
      </div>
      <div class="clr"></div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Invite Friends to Play</span>          </h2>
          <div class="clr"></div>
          
            <p>Insert your details below and invite your friends to play with you.</br>
<p><?php
//create_cat.php
include 'connect.php';
include 'header.php';
echo '<p>';

$sql = "SELECT
			topic_id,
			topic_subject
		FROM
			topics
		WHERE
			topics.topic_id = " . mysql_real_escape_string($_GET['id']);
			
$result = mysql_query($sql);

if(!$result)
{
	echo 'The topic could not be displayed, please try again later.';
}
else
{
	if(mysql_num_rows($result) == 0)
	{
		echo 'This topic doesn&prime;t exist.';
	}
	else
	{
		while($row = mysql_fetch_assoc($result))
		{
			//display post data
			echo '<table class="topic" border="1">
					<tr>
						<th colspan="2">' . $row['topic_subject'] . '</th>
					</tr>';
		
			//fetch the posts from the database
			$posts_sql = "SELECT
						posts.post_topic,
						posts.post_content,
						posts.post_date,
						posts.post_by,
						friendship_system_users_table.id,
						friendship_system_users_table.username
					FROM
						posts
					LEFT JOIN
						friendship_system_users_table
					ON
						posts.post_by = friendship_system_users_table.id
					WHERE
						posts.post_topic = " . mysql_real_escape_string($_GET['id']);
						
			$posts_result = mysql_query($posts_sql);
			
			if(!$posts_result)
			{
				echo '<tr><td>The posts could not be displayed, please try again later.</tr></td></table>';
			}
			else
			{
			
				while($posts_row = mysql_fetch_assoc($posts_result))
				{
					echo '<tr class="topic-post">
							<td class="post1">' . $posts_row['username'] . '<br/>' . date('d-m-Y H:i', strtotime($posts_row['post_date'])) . '</td>
							
							<td class="post2">' . htmlentities(stripslashes($posts_row['post_content'])) . '</td>
							
						  </tr>';
				}
			}
			
			if(!isset($_SESSION['signed_in']))
			{
				echo '<tr><td colspan=2>You must be <a href="login.php">signed in</a> to reply. You can also <a href="signup.php">sign up</a> for an account.';
			}
			else
			{
				//show reply box
				echo '<tr><td colspan="2"><h2>Reply:</h2><br />
					<form method="post" action="reply.php?id=' . $row['topic_id'] . '">
						<textarea name="reply-content"></textarea><br /><br />
						<input type="submit" value="Submit reply" />
					</form></td></tr>';
			}
			
			//finish the table
			echo '</table>';
		}
	}
}
echo '</p>';
//include 'footer.php';
?>
            
         
          <div class="clr"></div>
        </div>
      </div>
      <div class="sidebar">
        <div class="searchform"></div>
 		
        <div class="img"><img src="images/logo.jpg" width="261" height="86" alt="" class="fl" /></div>
        
        <div class="clr"></div>
       <div class="gadget">
          <h2 class="star"><span>Sportsman</span></h2>
          <div class="clr"></div>
          <ul class="sb_menu">
            <li><a href="Sportsman_IFP.html">Invite Friends to Play</a></li>
            <li><a href="Sportsman_BC.html">Book Court</a></li>
          </ul>
        </div>
        <div class="gadget">
          <h2 class="star"><span>Users</span></h2>
          <div class="clr"></div>
          <ul class="ex_menu">
            <li><a href="Coach.html">Coach</a></li>
            <li><a href="Student.html">Student</a></li>
            <li><a href="Sportsman.html">Sportsman</a></li>
            <li><a href="Club.html">Club</a></li>
          </ul>
           <div class="clr"></div>
        <div class="img"><img src="images/img1.jpg" width="220" height="215" alt="" class="fl" /></div>
        </div>
       
      </div>
      <div class="clr"></div>
    </div>
  </div>
 <div class="fbg">
    <div class="fbg_resize">
      <div class="col c1">
        <h2><span>Image</span> Gallery</h2>
        <a href="#"><img src="images/gal1.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal2.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal3.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal4.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal5.jpg" width="75" height="75" alt="" class="gal" /></a> <a href="#"><img src="images/gal6.jpg" width="75" height="75" alt="" class="gal" /></a> </div>
      <div class="col c2">
         <h2><span>Our Services</h2>
        <p>We provide our best servicers to you.</p>
        <ul class="fbg_ul">
          <li><a href="#">Invite friends to play</a></li>
          <li><a href="#">Arrange tournaments</a></li>
          <li><a href="#">Find your coach</a></li>
        </ul>
      </div>
      <div class="col c3">
            <h2><span>Contact</span> Us</h2>
        <p>We are here to provide our best service to you.</p>
        <p class="contact_info">
           <span>Name : </span>Ridgecrest Asia (Pvt) Ltd.<br /> 
          <span>Address:</span>113, 5th Lane, Colombo 03.<br />
          <span>Telephone:</span>0094 11 2370876 <br />
          <span>FAX:</span>0094 11 2370878<br />
          <span>E-mail:</span> <a href="#">info@ridgecrest.lk</a> </p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="footer">
    <div class="footer_resize">
      <p class="lf">&copy; Copyright © 2013<a href="http://www.sportslk.com"> Ridgecrest, Inc. </a>All rights reserved.</p>
      <p class="rf">Design by Ridgecrest</p>
      <div style="clear:both;"></div>
    </div>
  </div>
</div>
<div class="end">
<div class="end_text">© 2013<a href="http://www.sportslk.com"> Ridgecrest, Inc. </a>All rights reserved.</div>
</body>
</html>
